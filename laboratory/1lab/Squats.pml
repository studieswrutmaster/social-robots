<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Squats" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="OnceAgain" src="OnceAgain/OnceAgain.dlg" />
    </Dialogs>
    <Resources />
    <Topics>
        <Topic name="OnceAgain_enu" src="OnceAgain/OnceAgain_enu.top" topicName="OnceAgain" language="en_US" />
    </Topics>
    <IgnoredPaths />
</Package>
